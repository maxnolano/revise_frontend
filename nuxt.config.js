import colors from 'vuetify/es5/util/colors'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - revise_frontend',
    title: 'revise_frontend',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/dotenv'
  ],

  publicRuntimeConfig: {
    SERVER_ADDRESS: process.env.SERVER_ADDRESS || 'second or value',
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/auth-next',
    '@nuxtjs/axios',
    // '@nuxtjs/auth',
    '@nuxtjs/vuetify',
    'nuxtjs-mdi-font',
    '@nuxtjs/dotenv',
    'cookie-universal-nuxt',
  ],

  // // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  // vuetify: {
  //   customVariables: ['~/assets/variables.scss'],
  //   theme: {
  //     dark: true,
  //     themes: {
  //       dark: {
  //         primary: colors.blue.darken2,
  //         accent: colors.grey.darken3,
  //         secondary: colors.amber.darken3,
  //         info: colors.teal.lighten1,
  //         warning: colors.amber.base,
  //         error: colors.deepOrange.accent4,
  //         success: colors.green.accent3
  //       }
  //     }
  //   }
  // },

  // Axios module config
  axios: {
    baseURL: process.env.API_URL,
    // need to change to this -> process.env.URL_API, and in .env file, need to write
    // smth like URL_API=http://127.0.0.1:8000/api 
  },

  auth: {
    strategies: {
      'laravelJWT': {
        provider: 'laravel/jwt',
        url: '/',
        endpoints: {
          login: {
            url: 'auth/login',
            method: 'post',
            propertyName: false,
          },
          user: {
            url: 'auth/user',
            method: 'get',
            propertyName: false,
          },
          logout: {
            url: 'auth/logout',
            method: 'post',
            propertyName: false,
          }
        },
        token: {
          property: 'access_token',
          maxAge: 20160 * 60//2 weeks times 60
        },
        refreshToken: {
          maxAge: 20160 * 60//2 weeks times 60
        },
      },
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
