export default () => ({
headers_users: [
  { text: 'id', value: 'id' },
  { text: 'username', value: 'username' },
  { text: 'first_name', value: 'first_name' },
  { text: 'last_name', value: 'last_name' },
  { text: 'display_name', value: 'display_name' },
  { text: 'phone', value: 'phone' },
  { text: 'email', value: 'email' },
  { text: "", value: "controls", sortable: false },
],
});
  