export default {
    setHeaders(state, val) {
      state.headers = val;
    },
    setCities(state, val) {
      state.cities = val;
    },
    setStores(state, val) {
      state.stores = val;
    },
    setStations(state, val) {
      state.stations = val;
    },
  };
  