export default {
    setHeadersAction({ commit }, val) {
      commit("setHeaders", val);
    },
    setHeadersKstAction({ commit }, val) {
      commit("setHeadersKst", val);
    },
  };
  