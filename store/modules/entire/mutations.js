export default {
    setHeaders(state, val) {
      state.headers = val;
    },
    setCities(state, val) {
      state.cities = val;
    },
    setStores(state, val) {
      state.stores = val;
    },
    setStatuses(state, val) {
      state.statuses = val;
    },
  };
  